/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.postgresdemo ;

import org.springframework.data.repository.CrudRepository ;

import com.example.postgresdemo.Evenement ;

/**
 *
 * @author formation
 */
public interface EvenementRepository extends CrudRepository<Evenement, Integer>{
 
}
