/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.postgresdemo ;

import org.springframework.data.repository.CrudRepository ;

import com.example.postgresdemo.Participant ;
/**
 *
 * @author formation
 */

public interface ParticipantRepository extends CrudRepository<Participant, Integer> {
    
}
