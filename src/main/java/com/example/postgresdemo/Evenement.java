/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.example.postgresdemo;

import java.util.*;
import com.example.postgresdemo.Participant ;
import javax.persistence.CascadeType;
import static javax.persistence.CascadeType.ALL;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import static org.hibernate.validator.internal.util.CollectionHelper.*;
/**
 *
 * @author formation
 */
@Entity
@Table( name = "evenement" )
public class Evenement {
     
    @Id
    @GeneratedValue(generator="increment")
    @GenericGenerator(name="increment", strategy="increment")
    @Column(name="numpers")
    private int num_pers ;
    @Column(name="intitule")
    private String intitule ;
    
    @Column(name="theme")
    private String theme ;

    @Column(name="date_debut")
     private String date_debut ;
       
    @Column(name="duree")
     private int duree ;
        
    @Column(name="nb_part_max")
     private int nb_part_max ;
     
    @Column(name="description")
     private String description ;
    
    @Column(name="organisateur")
     private String organisateur ;
    
    @Column(name="type_even")
     private String type_even ;
    
    @OneToMany(mappedBy = "evenement", cascade = CascadeType.ALL) 
    private List<Participant> participants ;
    
    Evenement() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public List<Participant> getParticipant() {
        return participants;
    }

    public void setParticipant(List<Participant> participant) {
        this.participants = participant ;
    }
    
    

    public Evenement(int num_pers, String intitule, String theme, String date_debut, int duree, int nb_part_max, String description, String organisateur, String type_even) {
        this.num_pers = num_pers;
        this.intitule = intitule;
        this.theme = theme;
        this.date_debut = date_debut;
        this.duree = duree;
        this.nb_part_max = nb_part_max;
        this.description = description;
        this.organisateur = organisateur;
        this.type_even = type_even;
    }

    public int getNum_pers() {
        return num_pers;
    }

    public String getIntitule() {
        return intitule;
    }

    public String getTheme() {
        return theme;
    }

    public String getDate_debut() {
        return date_debut;
    }

    public int getDuree() {
        return duree;
    }

    public int getNb_part_max() {
        return nb_part_max;
    }

    public String getDescription() {
        return description;
    }

    public String getOrganisateur() {
        return organisateur;
    }

    public String getType_even() {
        return type_even;
    }

    public void setNum_pers(int num_pers) {
        this.num_pers = num_pers;
    }

    public void setIntitule(String intitule) {
        this.intitule = intitule;
    }

    public void setTheme(String theme) {
        this.theme = theme;
    }

    public void setDate_debut(String date_debut) {
        this.date_debut = date_debut;
    }

    public void setDuree(int duree) {
        this.duree = duree;
    }

    public void setNb_part_max(int nb_part_max) {
        this.nb_part_max = nb_part_max;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setOrganisateur(String organisateur) {
        this.organisateur = organisateur;
    }

    public void setType_even(String type_even) {
        this.type_even = type_even;
    }
}